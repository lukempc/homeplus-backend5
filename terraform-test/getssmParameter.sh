aws ssm get-parameter --name "/database/homeplus/endpoint"  --region ap-southease-2 --with-decryption
aws ssm get-parameter --name "/database/homeplus/name"  --region ap-southease-2 --with-decryption
aws ssm get-parameter --name "/database/homeplus/password"  --region ap-southease-2 --with-decryption
aws ssm get-parameter --name "/database/homeplus/user"  --region ap-southease-2 --with-decryption