package com.homeplus.utils;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.homeplus.dtos.task.TaskGetDto;
import com.homeplus.models.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.OffsetDateTime;
import java.util.Date;

@Configuration
public class Utility {
    public TaskEntity buildTaskEntity(
            UserEntity userEntity,
            TaskerEntity taskerEntity,
            String title,
            String category,
            OffsetDateTime date,
            TimeSectionsEntity timeSectionsEntity,
            Long budget,
            String state,
            String suburb,
            String street,
            Long postcode,
            String house_type,
            Integer num_of_rooms,
            Integer num_of_bathrooms,
            Integer levels,
            Boolean lift,
            Boolean in_person,
            String item_name,
            String item_value,
            String description,
            TaskStatus task_status,
            OffsetDateTime created_time,
            OffsetDateTime updated_time
    ) {
        TaskEntity taskEntity = new TaskEntity();
        taskEntity.setUserEntity(userEntity);
        taskEntity.setTaskerEntity(taskerEntity);
        taskEntity.setTitle(title);
        taskEntity.setCategory(category);
        taskEntity.setDate(date);
        taskEntity.setTimeSectionsEntity(timeSectionsEntity);
        taskEntity.setBudget(budget);
        taskEntity.setState(state);
        taskEntity.setSuburb(suburb);
        taskEntity.setStreet(street);
        taskEntity.setPostcode(postcode);
        taskEntity.setHouse_type(house_type);
        taskEntity.setNum_of_rooms(num_of_rooms);
        taskEntity.setNum_of_bathrooms(num_of_bathrooms);
        taskEntity.setLevels(levels);
        taskEntity.setLift(lift);
        taskEntity.setIn_person(in_person);
        taskEntity.setItem_name(item_name);
        taskEntity.setItem_value(item_value);
        taskEntity.setDescription(description);
        taskEntity.setTask_status(task_status);
        taskEntity.setCreated_time(created_time);
        taskEntity.setUpdated_time(updated_time);
        return taskEntity;
    }

    public TaskGetDto buildTaskGetDto(
            UserEntity userEntity,
            TaskerEntity taskerEntity,
            String title,
            String category,
            OffsetDateTime date,
            TimeSectionsEntity timeSectionsEntity,
            Long budget,
            String state,
            String suburb,
            String street,
            Long postcode,
            String house_type,
            Integer num_of_rooms,
            Integer num_of_bathrooms,
            Integer levels,
            Boolean lift,
            Boolean in_person,
            String item_name,
            String item_value,
            String description,
            String review,
            Integer rating,
            String task_status,
            OffsetDateTime created_time,
            OffsetDateTime updated_time
    ) {
        TaskGetDto taskGetDto = new TaskGetDto();
        taskGetDto.setUserEntity(userEntity);
        taskGetDto.setTaskerEntity(taskerEntity);
        taskGetDto.setTitle(title);
        taskGetDto.setCategory(category);
        taskGetDto.setDate(date);
        taskGetDto.setTimeSectionsEntity(timeSectionsEntity);
        taskGetDto.setBudget(budget);
        taskGetDto.setState(state);
        taskGetDto.setSuburb(suburb);
        taskGetDto.setStreet(street);
        taskGetDto.setPostcode(postcode);
        taskGetDto.setHouse_type(house_type);
        taskGetDto.setNum_of_rooms(num_of_rooms);
        taskGetDto.setNum_of_bathrooms(num_of_bathrooms);
        taskGetDto.setLevels(levels);
        taskGetDto.setLift(lift);
        taskGetDto.setIn_person(in_person);
        taskGetDto.setItem_name(item_name);
        taskGetDto.setItem_value(item_value);
        taskGetDto.setDescription(description);
        taskGetDto.setReview(review);
        taskGetDto.setRating(rating);
        taskGetDto.setTask_status(task_status);
        taskGetDto.setCreated_time(created_time);
        taskGetDto.setUpdated_time(updated_time);
        return taskGetDto;
    }

    public UserEntity buildUserEntity(
            String email,
            String name,
            String password,
            String gender,
            String language,
            String state,
            String street,
            String postcode,
            Date date_of_birth,
            Integer mobile,
            Boolean is_tasker,
            String status,
            OffsetDateTime createdTime,
            OffsetDateTime updatedTime
    ) {
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(email);
        userEntity.setName(name);
        userEntity.setPassword(password);
        userEntity.setGender(gender);
        userEntity.setLanguage(language);
        userEntity.setState(state);
        userEntity.setStreet(street);
        userEntity.setPostcode(postcode);
        userEntity.setDate_of_birth(date_of_birth);
        userEntity.setMobile(mobile);
        userEntity.setIs_tasker(is_tasker);
        userEntity.setState(status);
        userEntity.setCreatedTime(createdTime);
        userEntity.setUpdatedTime(updatedTime);
        return userEntity;
    }

    public TaskerEntity buildTaskerEntity(
            UserEntity userEntity,
            String title,
            String skills_description,
            String certifications,
            String category,
            String bank_bsb,
            String bank_account,
            OffsetDateTime created_time,
            OffsetDateTime updated_time
    ) {
        TaskerEntity taskerEntity = new TaskerEntity();
        taskerEntity.setUserEntity(userEntity);
        taskerEntity.setTitle(title);
        taskerEntity.setSkills_description(skills_description);
        taskerEntity.setCertifications(certifications);
        taskerEntity.setCategory(category);
        taskerEntity.setBank_bsb(bank_bsb);
        taskerEntity.setBank_account(bank_account);
        taskerEntity.setCreated_time(created_time);
        taskerEntity.setUpdated_time(updated_time);
        return taskerEntity;
    }

    public TimeSectionsEntity buildTimeSectionsEntity(
            String title,
            String description
        ) {
        TimeSectionsEntity timeSectionsEntity = new TimeSectionsEntity();
        timeSectionsEntity.setTitle(title);
        timeSectionsEntity.setDescription(description);
        return timeSectionsEntity;
    }

    @Bean
    public ObjectMapper getObjectMapper() {
        return new ObjectMapper();
    }
}
