package com.homeplus.dtos.task;

import com.homeplus.models.TaskerEntity;
import com.homeplus.models.TimeSectionsEntity;
import com.homeplus.models.UserEntity;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
public class TaskGetDto {
    private Long id;

    private UserEntity userEntity;

    private TaskerEntity taskerEntity;

    private String title;

    private String category;

    private OffsetDateTime date;

    private TimeSectionsEntity timeSectionsEntity;

    private Long budget;

    private String price;

    private String state;

    private String street;

    private String suburb;

    private Long postcode;

    private String house_type;

    private Integer num_of_rooms;

    private Integer num_of_bathrooms;

    private Integer levels;

    private Boolean lift;

    private Boolean in_person;

    private String item_name;

    private String item_value;

    private String description;

    private String review;

    private Integer rating;

    private String task_status;

    private OffsetDateTime created_time;

    private OffsetDateTime updated_time;
}
