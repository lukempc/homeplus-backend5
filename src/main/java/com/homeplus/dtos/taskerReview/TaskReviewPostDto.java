package com.homeplus.dtos.taskerReview;

import com.homeplus.models.TaskEntity;
import com.homeplus.models.TaskerEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.OffsetDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TaskReviewPostDto {

    private TaskEntity taskEntity;

    private String review;

    private Integer rating;

    private final OffsetDateTime created_time = OffsetDateTime.now();

    private final OffsetDateTime updated_time = OffsetDateTime.now();
}
